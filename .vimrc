" https://gitlab.com/trobador/vim/raw/master/.vimrc

set nocompatible

filetype plugin on
filetype indent off
syntax on

set encoding=utf-8

set hidden
set wildmenu
set showcmd
set hlsearch
" auto change working dir (could have problems with some plugins)
set autochdir

set ignorecase
set smartcase
set magic

set whichwrap+=<,>,[,]
set backspace=2

set autoindent
set smartindent
set tabstop=4
set softtabstop=-1
set shiftwidth=0
set smarttab
set copyindent
set preserveindent

set foldmethod=indent
set foldlevelstart=99

set cscopetag

set nostartofline
set ruler

set visualbell

set number

set cursorline

set laststatus=2
set statusline=b%n:\ %<%F\ %h%m%r%=%-14.(%l,%c%V%)\ %P

set listchars=tab:╾─,trail:•
if has("patch-7.4.710")
	set listchars+=space:·
endif

set completeopt=menu,menuone

set sessionoptions=blank,buffers,curdir,folds,help,tabpages,winsize

let maplocalleader="\<Space>"

" Emacs users may find this more comfortable :)
cnoremap <C-g> <C-c>

" Toggle paste mode (\p aste)
nnoremap <silent> <Leader>p :set invpaste \| :echo &paste ? "PASTE MODE ON" : "PASTE MODE OFF"<CR>

" Buffer management mapping
nnoremap g<Space> <C-S-^>
nnoremap gl :ls<CR>
nnoremap gb :bn<CR>
nnoremap gB :bN<CR>

" Visual mode paste without overwriting register
xnoremap <expr> P 'pgv"'.v:register.'y'

" Create new tab using current buffer (\t ab)
nnoremap <silent> <Leader>t :tab split<CR>

" Clean search pattern (\c lean)
nnoremap <silent> <Leader>c :let @/ = ""<CR>

" Toggle visible tabs and spaces (\v isible)
nnoremap <silent> <Leader>v :setlocal list!<CR>

" Convert tab characters to space characters (\<Tab>)
nnoremap <silent> <Leader><Tab> :setlocal noexpandtab <Bar> :%retab!<CR>
vnoremap <silent> <Leader><Tab> :<C-U>setlocal noexpandtab <Bar> '<,'>retab!<CR>

" Convert tab characters to space characters (\<Space>)
nnoremap <silent> <Leader><Space> :setlocal expandtab <Bar> %retab<CR>
vnoremap <silent> <Leader><Space> :<C-U>setlocal expandtab <Bar> '<,'>retab<CR>

" Insert date in ISO 8601 format (\d ate)
nnoremap <silent> <Leader>d "=strftime('%FT%T%z')<CR>p

" sudo tee trick
cnoremap w!! w !sudo tee > /dev/null %

if has("gui_running")
	set guioptions-=T
	set guioptions-=e
	set lines=35 columns=110

	" set guifont=ProggyCleanTTSZBP\ 12
	set guifont=Source\ Code\ Pro\ Medium\ 10

	silent! colorscheme inkpot
else
	if v:version >= 704
		au ColorScheme * hi CursorLine cterm=NONE | hi CursorLineNr cterm=bold ctermfg=Red
	endif
	silent! colorscheme dilemma
endif

"
" All hope abandon, ye who enter here
"
silent! packloadall
redir => s:myplugins|silent filter /\/plugin\/\w\+\.vim$/ scriptnames|redir END

if s:myplugins =~# "fzf.vim" && has("patch-7.4.2008")
	let g:fzf_layout = { 'down': '10' }
	command! -bang -nargs=? -complete=dir Files call fzf#vim#files(<q-args>, fzf#vim#with_preview(), <bang>0)
	command! -bang -nargs=? -complete=dir GFiles call fzf#vim#gitfiles(<q-args>, fzf#vim#with_preview(), <bang>0)
	nnoremap <silent> <Leader>f :execute system('git rev-parse --is-inside-work-tree') =~ 'true' ? 'GFiles' : 'Files'<CR>
	nnoremap <silent> <Leader>g :Rg<CR>
	nnoremap gl :Buffers<CR>
endif

if s:myplugins =~# "NERD_tree.vim"
	" If more than one window and previous buffer was NERDTree, go back to it.
	autocmd BufEnter * if bufname('#') =~# "^NERD_tree_" && winnr('$') > 1 | b# | endif
	" Close vim if the only window left open is a NERDTree.
	autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
	let g:NERDTreeWinSize = 25
	let g:NERDTreeMinimalUI = 1
	let g:NERDTreeShowHidden = 1
	noremap <silent> <F8> :NERDTreeToggle<CR>
else
	" Fallback to netrw
	let g:netrw_winsize = -25
	let g:netrw_banner = 0
	let g:netrw_liststyle = 3
	noremap <silent> <F8> :Lexplore<CR>
endif

